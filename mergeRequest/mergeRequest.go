package mergeRequest

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"log"
	"os"
	"strconv"
	"strings"
)

func (mergeRequest *MergeRequest) BuildMergeRequestMessageText() string {
	// Load Configs
	MasterLiveBranchName := os.Getenv("MASTER_LIVE_BRANCH_NAME")
	if MasterLiveBranchName == "" {
		MasterLiveBranchName = "master"
	}
	JiraUrl := os.Getenv("JIRA_URL")

	// status message parts
	var statusText []string

	// set assignee message part
	if mergeRequest.Assignee != "" {
		statusText = append(
			statusText,
			":bust_in_silhouette: _"+mergeRequest.Assignee+"_",
		)
	}

	// set source branch message part
	branchStringArray := []string{
		"Source:*" + mergeRequest.SourceBranch + "*",
	}

	// set target branch message part
	targetBranchEmoji := ""
	isMasterBranch := utils.StringInSlice(
		strings.ToLower(mergeRequest.TargetBranch),
		strings.Split(MasterLiveBranchName, ","),
		true,
	)
	if isMasterBranch {
		targetBranchEmoji = ":anatomical_heart:"
	}
	branchStringArray = append(branchStringArray, "Target:*"+targetBranchEmoji+mergeRequest.TargetBranch+"*")

	// build issue links message part
	issueIds, err := utils.GetIssueID(mergeRequest.MergeRequestTitle + mergeRequest.MergeRequestDescription)
	if JiraUrl != "" && len(issueIds) > 0 && err == nil {
		issueList := "Issues:" + utils.GetListOfIssuesAsMarkdown(utils.GetIssueIdsWithUrlPrefixed(issueIds, JiraUrl))
		branchStringArray = append(branchStringArray, issueList)
	}

	// generate branch info text with separator message part
	branchInfoText := strings.Join(branchStringArray, " | ") + "\n"

	if utils.FindRegexInStrings(utils.NoBrainMarkingRegex, []string{mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription}) {
		mergeRequest.NoBrainer = 1
	}
	if utils.FindRegexInStrings(utils.WipMarkingRegex, []string{mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription}) {
		mergeRequest.Wip = 1
	}

	// prefix if MR is nobrainer
	var messagePrefix = ""
	if mergeRequest.NoBrainer == 1 {
		messagePrefix = ":no-brainer:"
	}
	// add WIP emoji
	if mergeRequest.Wip == 1 {
		messagePrefix = ":hammer_and_wrench:" + messagePrefix
	}

	// build approval message part
	if mergeRequest.Approvalcount > 0 {
		statusText = append(
			statusText,
			"*"+strconv.FormatInt(mergeRequest.Approvalcount, 10)+"* Approve::heavy_check_mark:",
		)
	}

	// build review count message part
	if mergeRequest.Reviews > 0 {
		statusText = append(
			statusText,
			"*"+strconv.Itoa(mergeRequest.Reviews)+"* :eyes:",
		)
	}

	// build commented status message part
	if mergeRequest.Commented == 1 {
		statusText = append(statusText, "Comment::speech_balloon:")
	}

	// overwrite most status text and only show merged info and assignee after final merge
	if mergeRequest.Closed == utils.MergeRequestMerged {
		statusText = []string{}

		if mergeRequest.Assignee != "" {
			statusText = append(
				statusText,
				":bust_in_silhouette: _"+mergeRequest.Assignee+"_",
			)
		}
		statusText = append(statusText, "Merged::merged:")

		// clear branchInfoText Line if MR is merged, to make the message height smaller
		branchInfoText = ""
	}

	// build status info text from all status message parts
	var statusInfoText = strings.Join(statusText, " | ")

	var messageTextParts []string

	// replace special chars in title
	var cleanedMergeRequestTitle = utils.ReplaceStrings(mergeRequest.MergeRequestTitle)

	messageTextParts = append(messageTextParts, "<"+mergeRequest.MergeRequestUrl+"|*"+cleanedMergeRequestTitle+"*>\n"+branchInfoText+statusInfoText)
	if messagePrefix != "" {
		// prepend
		messageTextParts = append([]string{messagePrefix}, messageTextParts...)
	}

	messageTextParts = append(messageTextParts, "| Pipeline: "+mergeRequest.PipelineStatus)

	var messageText = strings.Join(messageTextParts, " ")

	return messageText
}

func (mergeRequest *MergeRequest) SendSlackMessage(api *slack.Client, message string, attachment slack.MsgOption, blockSections ...slack.Block) (error, string) {

	headerText := slack.NewTextBlockObject("mrkdwn", message, false, false)
	headerSectionBlock := slack.NewSectionBlock(headerText, nil, nil, slack.SectionBlockOptionBlockID("mrTextBlock"))

	allBlocks := []slack.Block{headerSectionBlock}
	allBlocks = append(allBlocks, blockSections...)
	blockAttachment := slack.MsgOptionBlocks(allBlocks...)

	channelID, timestamp, err := api.PostMessage(mergeRequest.ChannelId, slack.MsgOptionText(message, false), slack.MsgOptionBlocks(headerSectionBlock), slack.MsgOptionDisableLinkUnfurl(), blockAttachment)
	if err != nil {
		fmt.Printf("%s\n", err)
		return err, ""
	}
	fmt.Printf("Message successfully sent to channel %s at %s \n", channelID, timestamp)
	return nil, timestamp
}

func (mergeRequest *MergeRequest) UpdateMergeRequestDbEntry(db *sql.DB) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET assignee=?, merge_request_title=?, merge_request_description=?, message=?, merge_commit_sha=? WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.Assignee, mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription, mergeRequest.Message, mergeRequest.MergeCommitSha, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}

func (mergeRequest *MergeRequest) DeleteMergeRequestFromDB(db *sql.DB) error {
	delSqlErr := utils.ExecToDB(db, "DELETE FROM messages WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if delSqlErr != nil {
		fmt.Printf("%s\n", delSqlErr)
	}
	delReviewerSqlErr := utils.ExecToDB(db, "DELETE FROM reviewer WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if delReviewerSqlErr != nil {
		fmt.Printf("%s\n", delReviewerSqlErr)
	}
	return delSqlErr
}
func (mergeRequest *MergeRequest) UpdateApprovalCount(db *sql.DB, increase bool) {
	queryUpdateSign := "+"
	additionalQueryCheck := " AND approvalcount >= 0"
	if !increase {
		queryUpdateSign = "-"
		additionalQueryCheck = " AND approvalcount > 0"
	}
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET approvalcount = approvalcount "+queryUpdateSign+" 1 WHERE merge_request_id=? AND project_id=? AND channel_id=?"+additionalQueryCheck, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}
func (mergeRequest *MergeRequest) ResetApprovalCount(db *sql.DB) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET approvalcount = 0 WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
	delReviewerSqlErr := utils.ExecToDB(db, "DELETE FROM reviewer WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if delReviewerSqlErr != nil {
		fmt.Printf("%s\n", delReviewerSqlErr)
	}
}

func (mergeRequest *MergeRequest) UpdateReviewCount(db *sql.DB, reviewerUsername string, increase bool) bool {
	query := `SELECT reviewer_username FROM reviewer WHERE channel_id=? AND merge_request_id=? AND project_id=? AND reviewer_username=?`

	returnVal := true

	rows, err := db.Query(query, mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.ProjectId, reviewerUsername)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var reviewer_username = ""
	for rows.Next() {
		err = rows.Scan(&reviewer_username)
		if err != nil {
			log.Fatal(err)
		}
	}

	queryUpdateSign := "+"
	additionalQueryCheck := " AND reviews >= 0"
	if !increase {
		queryUpdateSign = "-"
		additionalQueryCheck = " AND reviews > 0"
	}

	// do not increase review count if user already pressed review
	var skipIncrease = false
	if reviewer_username != "" && increase {
		skipIncrease = true
		returnVal = false
	}

	if !skipIncrease {
		sqlErr := utils.ExecToDB(db, "UPDATE messages SET reviews = reviews "+queryUpdateSign+" 1 WHERE merge_request_id=? AND project_id=? AND channel_id=?"+additionalQueryCheck, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
		if sqlErr != nil {
			fmt.Printf("%s\n", sqlErr)
		}
	}
	if reviewerUsername != "" {
		if increase {
			insertErr := utils.ExecToDB(db, "INSERT INTO reviewer(channel_id, merge_request_id, project_id, reviewer_username) VALUES( ?, ?, ?, ? )", mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.ProjectId, reviewerUsername)
			if insertErr != nil {
				fmt.Printf("%s\n", insertErr)
			}
		} else {
			delSqlErr := utils.ExecToDB(db, "DELETE FROM reviewer WHERE channel_id=? AND merge_request_id=? AND project_id=? AND reviewer_username=?", mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.ProjectId, reviewerUsername)
			if delSqlErr != nil {
				fmt.Printf("%s\n", delSqlErr)
			}
		}
	}

	return returnVal
}

func (mergeRequest *MergeRequest) MarkDBEntryState(db *sql.DB, close int) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET closed=? WHERE merge_request_id=? AND project_id=? AND channel_id=?", close, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}

/*func (mergeRequest *MergeRequest) SetMergeStatus(db *sql.DB, mergeStatus string) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET merge_status = ? WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeStatus, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}*/

func (mergeRequest *MergeRequest) SetCommentedStatus(db *sql.DB) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET commented = 1 WHERE merge_request_id=? AND project_id=? AND channel_id=?", mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}

func (mergeRequest *MergeRequest) SetPipelineStatus(db *sql.DB, pipelineStatus string) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET pipeline_status=? WHERE merge_request_id=? AND project_id=? AND channel_id=?", pipelineStatus, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}

func (mergeRequest *MergeRequest) UpdateLastCommit(db *sql.DB, lastCommit string) {
	sqlErr := utils.ExecToDB(db, "UPDATE messages SET last_commit=? WHERE merge_request_id=? AND project_id=? AND channel_id=?", lastCommit, mergeRequest.MergeRequestId, mergeRequest.ProjectId, mergeRequest.ChannelId)
	if sqlErr != nil {
		fmt.Printf("%s\n", sqlErr)
	}
}
