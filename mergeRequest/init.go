package mergeRequest

import (
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"log"
)

type MergeRequest struct {
	Message                 string
	ChannelId               string
	MsgTimestamp            string
	MergeRequestId          int64
	MergeRequestIId         int64
	MergeRequestTitle       string
	MergeRequestDescription string
	MergeRequestUrl         string
	SourceBranch            string
	TargetBranch            string
	ProjectId               int64
	RepoUrl                 string
	Approvalcount           int64
	Assignee                string
	Closed                  int
	MergeStatus             string
	Reviews                 int
	Commented               int
	NoBrainer               int
	Wip                     int
	PipelineStatus          string
	LastCommit              string
	MergeCommitSha          string
}

// GetFromDB Fetch mergeRequest object from Database
//
//goland:noinspection GoSnakeCaseUsage,SpellCheckingInspection
func GetFromDB(db *sql.DB, channelId string, mergeRequestID int64, mergeCommitSha string, projectId int64) (error, *MergeRequest) {
	query := `SELECT channel_id, msg_timestamp, approvalcount, merge_request_iid, assignee, closed, merge_status, reviews, message, commented, nobrainer, merge_request_title, merge_request_description, merge_request_url, pipeline_status, last_commit, merge_commit_sha FROM messages WHERE (merge_request_id=? OR (merge_commit_sha=? AND merge_commit_sha!='')) AND project_id=? AND channel_id=?`

	rows, err := db.Query(query, mergeRequestID, mergeCommitSha, projectId, channelId)
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	defer rows.Close()
	for rows.Next() {
		var channel_id string
		var msg_timestamp string
		var approvalcount int64
		var merge_request_iid int64
		var assignee string
		var closed int
		var merge_status sql.NullString
		var reviews int
		var message string
		var commented int
		var nobrainer int
		var merge_request_title string
		var merge_request_description string
		var merge_request_url string
		var pipeline_status sql.NullString
		var last_commit sql.NullString
		var merge_commit_sha sql.NullString

		err = rows.Scan(&channel_id, &msg_timestamp, &approvalcount, &merge_request_iid, &assignee, &closed, &merge_status, &reviews, &message, &commented, &nobrainer, &merge_request_title, &merge_request_description, &merge_request_url, &pipeline_status, &last_commit, &merge_commit_sha)
		if err != nil {
			log.Fatal(err)
			return err, nil
		}

		fmt.Println(channel_id, msg_timestamp, approvalcount, merge_request_iid, assignee)

		returnEntry := &MergeRequest{
			Message:                 message,
			ChannelId:               channel_id,
			MsgTimestamp:            msg_timestamp,
			MergeRequestId:          mergeRequestID,
			MergeRequestIId:         merge_request_iid,
			ProjectId:               projectId,
			Approvalcount:           approvalcount,
			Assignee:                assignee,
			Closed:                  closed,
			MergeStatus:             merge_status.String,
			Reviews:                 reviews,
			Commented:               commented,
			NoBrainer:               nobrainer,
			MergeRequestTitle:       merge_request_title,
			MergeRequestDescription: merge_request_description,
			MergeRequestUrl:         merge_request_url,
			PipelineStatus:          pipeline_status.String,
			LastCommit:              last_commit.String,
			MergeCommitSha:          merge_commit_sha.String,
		}

		return nil, returnEntry
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
		return err, nil
	}
	return errors.New("sql: no Rows available"), nil
}

func (mergeRequest *MergeRequest) SendMergeRequestMessage(slackApi *slack.Client, db *sql.DB, attachment slack.MsgOption, blockSections ...slack.Block) {
	messageText := mergeRequest.BuildMergeRequestMessageText()

	err, _ := GetFromDB(db, mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.MergeCommitSha, mergeRequest.ProjectId)

	// if no row was found
	if err != nil {
		sendErr, newMsgTimestamp := mergeRequest.SendSlackMessage(slackApi, messageText, attachment, blockSections...)
		if sendErr != nil {
			fmt.Printf("%s\n", sendErr)
		}
		insertErr := utils.ExecToDB(db, "INSERT INTO messages(channel_id, msg_timestamp, merge_request_id, merge_request_title, merge_request_description, merge_request_url, project_id, merge_request_iid, assignee, message, pipeline_status, merge_status, last_commit, merge_commit_sha) VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )", mergeRequest.ChannelId, newMsgTimestamp, mergeRequest.MergeRequestId, mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription, mergeRequest.MergeRequestUrl, mergeRequest.ProjectId, mergeRequest.MergeRequestIId, mergeRequest.Assignee, messageText, "", mergeRequest.MergeStatus, mergeRequest.LastCommit, mergeRequest.MergeCommitSha)
		if insertErr != nil {
			fmt.Printf("%s\n", insertErr)
		}
	}
}

func (mergeRequest *MergeRequest) UpdateMergeRequestMessage(slackApi *slack.Client, db *sql.DB, attachment slack.MsgOption, blockSections ...slack.Block) {
	messageText := mergeRequest.BuildMergeRequestMessageText()

	err, _ := GetFromDB(db, mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.MergeCommitSha, mergeRequest.ProjectId)

	// if row was found
	if err == nil {

		headerText := slack.NewTextBlockObject("mrkdwn", messageText, false, false)
		headerSectionBlock := slack.NewSectionBlock(headerText, nil, nil, slack.SectionBlockOptionBlockID("mrTextBlock"))

		allBlocks := []slack.Block{headerSectionBlock}
		allBlocks = append(allBlocks, blockSections...)
		blockAttachment := slack.MsgOptionBlocks(allBlocks...)

		_, _, _, updateErr := slackApi.UpdateMessage(mergeRequest.ChannelId, mergeRequest.MsgTimestamp, slack.MsgOptionText(messageText, false), slack.MsgOptionBlocks(headerSectionBlock), slack.MsgOptionDisableLinkUnfurl(), blockAttachment)

		mergeRequest.Message = messageText
		mergeRequest.UpdateMergeRequestDbEntry(db)
		if updateErr != nil {
			fmt.Printf("%s\n", updateErr)
		}
	}
}

func (mergeRequest *MergeRequest) DeleteMergeRequestMessage(slackApi *slack.Client, db *sql.DB) {
	err, dbEntry := GetFromDB(db, mergeRequest.ChannelId, mergeRequest.MergeRequestId, mergeRequest.MergeCommitSha, mergeRequest.ProjectId)
	if err == nil {
		_, _, delErr := slackApi.DeleteMessage(mergeRequest.ChannelId, dbEntry.MsgTimestamp)
		if delErr != nil {
			fmt.Printf("%s\n", delErr)
		}
		_ = mergeRequest.DeleteMergeRequestFromDB(db)
	}
}
