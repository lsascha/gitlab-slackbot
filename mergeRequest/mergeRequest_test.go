package mergeRequest

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"log"
	"os"
	"regexp"
	"strconv"
	"testing"
)

var testMergeRequestEntry = MergeRequest{
	ChannelId:               "123",
	MergeRequestId:          123,
	MergeRequestIId:         1234,
	MergeRequestTitle:       "ABC-123 mergeRequestTitle",
	MergeRequestDescription: "mergeRequestDescription no-brainer CDE-456",
	MergeRequestUrl:         "http://mergeRequestUrl",
	ProjectId:               12345,
	Approvalcount:           1,
	Assignee:                "mergeRequestAssignee",
	Reviews:                 1,
	LastCommit:              "lastCommit",
	Closed:                  utils.MergeRequestOpen,
	PipelineStatus:          "pipelineStatus",
	MergeStatus:             "mergeRequestStatus",
	SourceBranch:            "mergeRequestSourceBranch",
	TargetBranch:            "mergeRequestTargetBranch",
	Commented:               1,
	MergeCommitSha:          "mergeCommitSha",
	// non persistent entries
	RepoUrl:      "",
	Message:      "",
	MsgTimestamp: "",
	NoBrainer:    1,
	Wip:          1,
}

var db = initDb()

func initDb() *sql.DB {
	db, err := sql.Open("sqlite3", "file::memory:?mode=rwc&_journal_mode=WAL&_busy_timeout=5000")

	if err != nil {
		log.Fatalf("Error in opening Database: %s.", err)
		return db
	}

	sqlStmt := `
		DROP TABLE IF EXISTS messages;
		DROP TABLE IF EXISTS reviewer;
	`
	err = utils.ExecToDB(db, sqlStmt)
	if err != nil {
		log.Fatalf("Error in initializing Database: %s.", err)
		return db
	}
	sqlStmt = `
		create table if not exists messages (id integer not null primary key, message text, channel_id text, msg_timestamp text, merge_request_id integer not null, merge_request_iid integer not null, merge_request_title text, merge_request_description text, merge_request_url text, project_id integer not null, repo_url text, approvalcount integer unsigned default 0, assignee text, closed integer unsigned default 0, merge_status text, reviews integer unsigned default 0, commented integer unsigned default 0, nobrainer integer unsigned default 0, pipeline_status text, last_commit text, merge_commit_sha text);
	`
	err = utils.ExecToDB(db, sqlStmt)
	if err != nil {
		log.Fatalf("Error in initializing Database: %s.", err)
		return db
	}
	sqlStmt = `
		create table if not exists reviewer (id integer not null primary key, channel_id text, merge_request_id integer not null, project_id integer not null, reviewer_username text);
	`
	err = utils.ExecToDB(db, sqlStmt)
	if err != nil {
		log.Fatalf("Error in initializing Database: %s.", err)
		return db
	}

	return db
}

func TestBuildMergeRequestMessageText(t *testing.T) {
	err := os.Setenv("JIRA_URL", "https://test.atlassian.net/browse/")
	if err != nil || os.Getenv("JIRA_URL") != "https://test.atlassian.net/browse/" {
		t.Fatalf(`Error setting Environment variable JIRA_URL: %q`, err)
	}

	mergeRequestMessageTest := testMergeRequestEntry.BuildMergeRequestMessageText()
	// Test for message to contain assignee
	want := regexp.MustCompile(`\b` + "_" + testMergeRequestEntry.Assignee + "_" + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// Test for message to contain source branch
	want = regexp.MustCompile(`\b` + testMergeRequestEntry.SourceBranch + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// Test for message to contain target branch
	want = regexp.MustCompile(`\b` + testMergeRequestEntry.TargetBranch + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// test if WIP emoji is added for WIP MR
	want = regexp.MustCompile(":hammer_and_wrench:")
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// test if no-brainer emoji is added for no-brainer MR
	want = regexp.MustCompile(":no-brainer:")
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// test if message contains approval count
	want = regexp.MustCompile(`\*` + strconv.FormatInt(testMergeRequestEntry.Approvalcount, 10) + `\* Approve::heavy_check_mark:`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// test if message contains review count
	want = regexp.MustCompile(`\*` + strconv.Itoa(testMergeRequestEntry.Reviews) + `\* :eyes:`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// test if comment emoji is added for commented MR
	want = regexp.MustCompile("Comment::speech_balloon:")
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// Test for message to contain merge request url
	want = regexp.MustCompile(`\b` + testMergeRequestEntry.MergeRequestUrl + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// Test for message to contain merge request title
	want = regexp.MustCompile(`\b` + testMergeRequestEntry.MergeRequestTitle + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	// Test for message to contain Jira issue link
	want = regexp.MustCompile(`\b` + os.Getenv("JIRA_URL") + "ABC-123" + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
	want = regexp.MustCompile(`\b` + os.Getenv("JIRA_URL") + "CDE-456" + `\b`)
	if !want.MatchString(mergeRequestMessageTest) {
		t.Fatalf(`BuildMergeRequestMessageText(testDbEntry) = %q, want match for %#q`, mergeRequestMessageTest, want)
	}
}

func insertMessageToDb(t *testing.T, db *sql.DB, dbEntry MergeRequest) {
	err := utils.ExecToDB(db, `
		INSERT INTO messages(
		                     channel_id,
		                     msg_timestamp,
		                     approvalcount,
		                     merge_request_id,
		                     merge_request_title,
		                     merge_request_description,
		                     merge_request_url,
		                     project_id,
		                     merge_request_iid,
		                     assignee,
		                     message,
		                     pipeline_status,
		                     merge_status,
		                     last_commit,
		                     closed,
		                     reviews,
		                     commented,
		                     merge_commit_sha
		                     )
			VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )
		`, dbEntry.ChannelId, dbEntry.MsgTimestamp, dbEntry.Approvalcount, dbEntry.MergeRequestId, dbEntry.MergeRequestTitle, dbEntry.MergeRequestDescription, dbEntry.MergeRequestUrl, dbEntry.ProjectId, dbEntry.MergeRequestIId, dbEntry.Assignee, dbEntry.Message, dbEntry.PipelineStatus, dbEntry.MergeStatus, dbEntry.LastCommit, dbEntry.Closed, dbEntry.Reviews, dbEntry.Commented, dbEntry.MergeCommitSha)

	if err != nil {
		t.Fatalf(`utils.ExecToDB failed: %q`, err)
	}
}

//goland:noinspection GoSnakeCaseUsage
func TestExecToDB(t *testing.T) {
	insertMessageToDb(t, db, testMergeRequestEntry)

	// test if values could be read from db again
	query := `SELECT channel_id, msg_timestamp, approvalcount, merge_request_id, merge_request_iid, assignee, closed, merge_status, reviews, message, commented, nobrainer, merge_request_title, merge_request_description, merge_request_url, pipeline_status, last_commit, merge_commit_sha FROM messages WHERE merge_request_id=? AND project_id=? AND channel_id=?`
	rows, err := db.Query(query, testMergeRequestEntry.MergeRequestId, testMergeRequestEntry.ProjectId, testMergeRequestEntry.ChannelId)
	if err != nil {
		t.Fatalf(`reading from db failed: %q`, err)
	}

	count := 0
	defer rows.Close()
	for rows.Next() {
		count++

		var channel_id string
		var msg_timestamp string
		var approvalcount int64
		var merge_request_id int64
		var merge_request_iid int64
		var assignee string
		var closed int
		var merge_status sql.NullString
		var reviews int
		var message string
		var commented int
		var nobrainer int
		var merge_request_title string
		var merge_request_description string
		var merge_request_url string
		var pipeline_status sql.NullString
		var last_commit sql.NullString
		var merge_commit_sha sql.NullString

		err = rows.Scan(&channel_id, &msg_timestamp, &approvalcount, &merge_request_id, &merge_request_iid, &assignee, &closed, &merge_status, &reviews, &message, &commented, &nobrainer, &merge_request_title, &merge_request_description, &merge_request_url, &pipeline_status, &last_commit, &merge_commit_sha)
		if err != nil {
			t.Fatalf(`reading rows from db failed: %q`, err)
		}

		returnedEntry := MergeRequest{
			ChannelId:               channel_id,
			MergeRequestId:          merge_request_id,
			MergeRequestIId:         merge_request_iid,
			MergeRequestTitle:       merge_request_title,
			MergeRequestDescription: merge_request_description,
			MergeRequestUrl:         merge_request_url,
			ProjectId:               testMergeRequestEntry.ProjectId,
			Approvalcount:           approvalcount,
			Assignee:                assignee,
			Reviews:                 reviews,
			LastCommit:              last_commit.String,
			Closed:                  closed,
			PipelineStatus:          pipeline_status.String,
			MergeStatus:             merge_status.String,
			SourceBranch:            testMergeRequestEntry.SourceBranch,
			TargetBranch:            testMergeRequestEntry.TargetBranch,
			Commented:               commented,
			// unused entries
			RepoUrl:        testMergeRequestEntry.RepoUrl,
			Message:        testMergeRequestEntry.Message,
			MsgTimestamp:   msg_timestamp,
			NoBrainer:      testMergeRequestEntry.NoBrainer,
			Wip:            testMergeRequestEntry.Wip,
			MergeCommitSha: merge_commit_sha.String,
		}

		if returnedEntry != testMergeRequestEntry {
			t.Fatalf(`
				returned db entry is different from saved one: %q
				returnedEntry: %q != testMergeRequestEntry: %q
			`, err, &returnedEntry, &testMergeRequestEntry)
		}
	}
	err = rows.Err()
	if err != nil {
		t.Fatalf(`reading rows from db failed: %q`, err)
	}
	if count != 1 {
		t.Fatalf(`reading from db failed: expected 1 row, got %d`, count)
	}
}

func TestDeleteMergeRequestFromDB(t *testing.T) {
	copyDbEntry := testMergeRequestEntry
	copyDbEntry.ChannelId = "456"
	copyDbEntry.MergeRequestId = 456

	insertMessageToDb(t, db, copyDbEntry)

	// test if values are inserted
	query := `SELECT * FROM messages WHERE merge_request_id=? AND project_id=? AND channel_id=?`
	rows, err := db.Query(query, copyDbEntry.MergeRequestId, copyDbEntry.ProjectId, copyDbEntry.ChannelId)
	if err != nil {
		t.Fatalf(`reading from db failed: %q`, err)
	}

	// count rows
	count := 0
	for rows.Next() {
		count++
	}
	if count != 1 {
		t.Fatalf(`reading from db failed: expected 1 row, got %d`, count)
	}
	rows.Close()

	// delete message from db
	err = copyDbEntry.DeleteMergeRequestFromDB(db)
	if err != nil {
		t.Fatalf(`deleting from db failed: %q`, err)
	}

	// test if values are deleted
	query = `SELECT * FROM messages WHERE merge_request_id=? AND project_id=? AND channel_id=?`
	rows, err = db.Query(query, copyDbEntry.MergeRequestId, copyDbEntry.ProjectId, copyDbEntry.ChannelId)
	if err != nil {
		t.Fatalf(`reading from db failed: %q`, err)
	}

	// count rows
	count = 0
	for rows.Next() {
		count++
	}
	if count != 0 {
		t.Fatalf(`reading from db failed: expected 0 row after deletion, got %d`, count)
	}
	rows.Close()
}

func TestSetPipelineStatus(t *testing.T) {
	copyDbEntry := testMergeRequestEntry
	copyDbEntry.ChannelId = "456"
	copyDbEntry.MergeRequestId = 456

	insertMessageToDb(t, db, copyDbEntry)

	copyDbEntry.SetPipelineStatus(db, "success")

	err, mergeRequestEntry := GetFromDB(db, copyDbEntry.ChannelId, copyDbEntry.MergeRequestId, copyDbEntry.MergeCommitSha, copyDbEntry.ProjectId)
	if err != nil {
		t.Fatalf(`utils.GetFromDB failed: %q`, err)
	}
	if mergeRequestEntry.PipelineStatus != "success" {
		t.Fatalf(`pipeline status should be success but is ` + mergeRequestEntry.PipelineStatus)
	}
}

func TestFetchByMergeCommitSha(t *testing.T) {
	copyDbEntry := testMergeRequestEntry
	copyDbEntry.MergeRequestId = 000

	err, mergeRequestEntry := GetFromDB(db, copyDbEntry.ChannelId, copyDbEntry.MergeRequestId, copyDbEntry.MergeCommitSha, copyDbEntry.ProjectId)
	if err != nil {
		t.Fatalf(`utils.GetFromDB failed: %q`, err)
	}
	if mergeRequestEntry.MergeCommitSha != copyDbEntry.MergeCommitSha || mergeRequestEntry.MergeCommitSha == "" {
		t.Fatalf(`merge commit sha should be ` + copyDbEntry.MergeCommitSha + ` but is ` + mergeRequestEntry.MergeCommitSha)
	}
}

func TestMarkDBEntryState(t *testing.T) {
	//copyDbEntry := testMergeRequestEntry
	insertMessageToDb(t, db, testMergeRequestEntry)

	testMergeRequestEntry.MarkDBEntryState(db, utils.MergeRequestMerged)

	err, mergeRequestEntry := GetFromDB(db, testMergeRequestEntry.ChannelId, testMergeRequestEntry.MergeRequestId, testMergeRequestEntry.MergeCommitSha, testMergeRequestEntry.ProjectId)
	if err != nil {
		t.Fatalf(`utils.GetFromDB failed: %q`, err)
	}
	if mergeRequestEntry.Closed != utils.MergeRequestMerged {
		t.Fatalf(`merge closed should be ` + strconv.Itoa(utils.MergeRequestMerged) + ` but is ` + strconv.Itoa(mergeRequestEntry.Closed))
	}
}
