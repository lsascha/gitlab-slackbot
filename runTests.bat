@echo off

Rem # find package prefix
FOR /F "tokens=* USEBACKQ" %%g IN (`go list -m -f "{{.Path}}"`) do (SET "PACKAGE_PREFIX=%%g")

Rem # run 'go test' on all local packages
FOR /F "tokens=* USEBACKQ" %%g IN (`go list %PACKAGE_PREFIX%/...`) do (go test -v %%g)
