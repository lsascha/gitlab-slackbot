package gitlabUtils

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/settings"
)

type GitlabUtils struct {
	Api *slack.Client
	Db  *sql.DB
}

var Settings settings.Settings

func init() {
	Settings = Settings.NewFromEnv()
}

func New(Api *slack.Client, Db *sql.DB) GitlabUtils {
	return GitlabUtils{
		Api: Api,
		Db:  Db,
	}
}
