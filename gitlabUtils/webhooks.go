package gitlabUtils

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/go-playground/webhooks/v6/gitlab"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/mergeRequest"
	"gitlab.com/lsascha/gitlab-slackbot/slackUtils"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
)

func (data *GitlabUtils) WebhookDataHandler(payload interface{}) {
	var mergeRequestId int64
	var mergeRequestIid int64
	var projectId int64
	var mergeRequestTitle string
	var mergeRequestUrl string
	var approvalCount int64
	var mergeRequestAssignee string
	//var isNoBrainer int
	//var isWip int
	var lastCommit string
	var mergeCommitSha string
	var mergeRequestStatus string
	var mergeRequestAction string
	var mergeRequestDescription string
	var mergeRequestSourceBranch string
	var mergeRequestTargetBranch string
	var isCommented bool
	var pipelineStatus string

	switch payload.(type) {
	case gitlab.MergeRequestEventPayload:
		mergeRequestPayload := payload.(gitlab.MergeRequestEventPayload)
		mergeRequestId = mergeRequestPayload.ObjectAttributes.ID
		mergeRequestIid = mergeRequestPayload.ObjectAttributes.IID
		projectId = mergeRequestPayload.ObjectAttributes.ProjectID
		if projectId == 0 {
			projectId = mergeRequestPayload.ObjectAttributes.SourceProjectID
		}
		mergeRequestTitle = mergeRequestPayload.ObjectAttributes.Title
		mergeRequestUrl = mergeRequestPayload.ObjectAttributes.URL

		lastCommit = mergeRequestPayload.ObjectAttributes.LastCommit.ID
		if mergeRequestPayload.ObjectAttributes.MergeCommitSha != "" {
			mergeCommitSha = mergeRequestPayload.ObjectAttributes.MergeCommitSha
		}

		//--
		mergeRequestStatus = mergeRequestPayload.ObjectAttributes.MergeStatus
		mergeRequestAction = mergeRequestPayload.ObjectAttributes.Action
		mergeRequestDescription = mergeRequestPayload.ObjectAttributes.Description
		mergeRequestSourceBranch = mergeRequestPayload.ObjectAttributes.SourceBranch
		mergeRequestTargetBranch = mergeRequestPayload.ObjectAttributes.TargetBranch

		if len(mergeRequestPayload.Assignees) > 0 {
			mergeRequestAssignee = mergeRequestPayload.Assignees[0].Name
		}
		// if no assignee, then use the user who created, merged or closed the merge request
		if mergeRequestAssignee == "" && mergeRequestPayload.User.Name != "" &&
			(mergeRequestAction == "reopen" || mergeRequestAction == "open" ||
				mergeRequestAction == "close" || mergeRequestAction == "closed" ||
				mergeRequestAction == "merge") {
			mergeRequestAssignee = mergeRequestPayload.User.Name
		}

		fmt.Println("Merge Request Event: Title:" + mergeRequestTitle + " URL:" + mergeRequestUrl + " Action:" + mergeRequestAction + " Last-Commit:" + lastCommit + " mergeCommitSha:" + mergeCommitSha)
		break

	case gitlab.CommentEventPayload:
		commentEventPayload := payload.(gitlab.CommentEventPayload)
		if commentEventPayload.ObjectAttributes.NotebookType == "MergeRequest" && commentEventPayload.MergeRequest.ID != 0 {
			mergeRequestId = commentEventPayload.MergeRequest.ID
			mergeRequestIid = commentEventPayload.MergeRequest.IID
			mergeRequestStatus = commentEventPayload.MergeRequest.MergeStatus
			projectId = commentEventPayload.ProjectID
			mergeRequestSourceBranch = commentEventPayload.MergeRequest.SourceBranch
			mergeRequestTargetBranch = commentEventPayload.MergeRequest.TargetBranch
			mergeRequestTitle = commentEventPayload.MergeRequest.Title
			mergeRequestDescription = commentEventPayload.MergeRequest.Description
			mergeRequestUrl = strings.Split(commentEventPayload.ObjectAttributes.URL, "#")[0]

			commentText := commentEventPayload.ObjectAttributes.Note
			commentUsername := commentEventPayload.User.UserName

			// ignore comment counting if comment is made by ignored username
			if !strings.HasPrefix(commentText, utils.MergeRequestBotMergeCommentText) || !utils.StringInSlice(commentUsername, Settings.GetIgnoredUsernamesSlice(), true) {
				isCommented = true
				_, mergeRequestEntry := mergeRequest.GetFromDB(data.Db, Settings.ChannelId, mergeRequestId, "", projectId)
				mergeRequestEntry.SetCommentedStatus(data.Db)

				fmt.Println("Merge Request Comment Event: Title:" + mergeRequestTitle + " URL:" + mergeRequestUrl + " Action:" + mergeRequestAction + " Comment-by:" + commentUsername)
			}
		}
		break

	case gitlab.PipelineEventPayload:
		pipelineEventPayload := payload.(gitlab.PipelineEventPayload)
		projectId = pipelineEventPayload.Project.ID
		mergeRequestId = pipelineEventPayload.MergeRequest.ID
		mergeRequestIid = pipelineEventPayload.MergeRequest.IID

		tmpMergeCommitSha := ""
		if pipelineEventPayload.Commit.ID != "" {
			tmpMergeCommitSha = pipelineEventPayload.Commit.ID
		} else if pipelineEventPayload.ObjectAttributes.SHA != "" {
			tmpMergeCommitSha = pipelineEventPayload.ObjectAttributes.SHA
		}

		// pipelineStatus can be one of: created, waiting_for_resource, preparing, pending, running, success, failed, canceled, skipped, manual, scheduled
		pipelineStatus = pipelineEventPayload.ObjectAttributes.Status

		mergeRequestStatus = pipelineEventPayload.MergeRequest.MergeStatus
		mergeRequestSourceBranch = pipelineEventPayload.MergeRequest.SourceBranch
		mergeRequestTargetBranch = pipelineEventPayload.MergeRequest.TargetBranch
		mergeRequestTitle = pipelineEventPayload.MergeRequest.Title
		mergeRequestDescription = pipelineEventPayload.MergeRequest.Description
		mergeRequestUrl = pipelineEventPayload.MergeRequest.URL

		_, mergeRequestEntry := mergeRequest.GetFromDB(data.Db, Settings.ChannelId, mergeRequestId, tmpMergeCommitSha, projectId)
		if mergeRequestEntry != nil {
			// get mergeRequestId from DB if not set, to update the correct entry later
			if mergeRequestEntry.MergeRequestId != 0 && mergeRequestId == 0 {
				mergeRequestId = mergeRequestEntry.MergeRequestId
			}

			// add some infos for log
			if mergeRequestEntry.MergeRequestTitle != "" && mergeRequestTitle == "" {
				mergeRequestTitle = mergeRequestEntry.MergeRequestTitle
			}
			if mergeRequestEntry.MergeRequestUrl != "" && mergeRequestUrl == "" {
				mergeRequestUrl = mergeRequestEntry.MergeRequestUrl
			}
			mergeRequestEntry.SetPipelineStatus(data.Db, pipelineStatus)
		} else {
			fmt.Println("No Merge Request Entry Found in DB.")
		}

		fmt.Println("Pipeline Event: Title:" + mergeRequestTitle + " URL:" + mergeRequestUrl + " Action:" + mergeRequestAction + " mergeCommitSha:" + mergeCommitSha + " Pipeline-Status:" + pipelineStatus)

		break
	}

	// only run if valid mergeRequestId is found
	if mergeRequestId > 0 {
		_, mergeRequestEntry := mergeRequest.GetFromDB(data.Db, Settings.ChannelId, mergeRequestId, mergeCommitSha, projectId)

		if mergeRequestEntry == nil {
			mergeRequestEntry = &mergeRequest.MergeRequest{
				ChannelId:               Settings.ChannelId,
				MergeRequestId:          mergeRequestId,
				MergeRequestIId:         mergeRequestIid,
				MergeRequestTitle:       mergeRequestTitle,
				MergeRequestDescription: mergeRequestDescription,
				MergeRequestUrl:         mergeRequestUrl,
				ProjectId:               projectId,
				Approvalcount:           approvalCount,
				Assignee:                mergeRequestAssignee,
				Reviews:                 0,
				LastCommit:              lastCommit,
				Closed:                  utils.MergeRequestOpen,
				PipelineStatus:          pipelineStatus,
				MergeStatus:             mergeRequestStatus,
				SourceBranch:            mergeRequestSourceBranch,
				TargetBranch:            mergeRequestTargetBranch,
			}
		} else {
			mergeRequestEntry.ChannelId = Settings.ChannelId
			mergeRequestEntry.ProjectId = projectId
			mergeRequestEntry.MergeRequestId = mergeRequestId
			mergeRequestEntry.MergeRequestIId = mergeRequestIid
			mergeRequestEntry.MergeRequestTitle = mergeRequestTitle
			mergeRequestEntry.MergeRequestDescription = mergeRequestDescription
			mergeRequestEntry.MergeRequestUrl = mergeRequestUrl
			mergeRequestEntry.SourceBranch = mergeRequestSourceBranch
			mergeRequestEntry.TargetBranch = mergeRequestTargetBranch
			if mergeRequestAssignee != "" {
				mergeRequestEntry.Assignee = mergeRequestAssignee
			}
			if mergeRequestStatus != "" {
				mergeRequestEntry.MergeStatus = mergeRequestStatus
			}
		}
		if mergeCommitSha != "" {
			mergeRequestEntry.MergeCommitSha = mergeCommitSha
		}
		// event specific mergeRequestEntry overwrites
		if isCommented {
			mergeRequestEntry.Commented = 1
		}
		if pipelineStatus != "" {
			mergeRequestEntry.PipelineStatus = pipelineStatus
		}

		var openNewMergeRequest = false

		// see https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#merge-request-events
		switch mergeRequestAction {
		case "approved", "approval":
			mergeRequestEntry.Approvalcount++
			mergeRequestEntry.UpdateApprovalCount(data.Db, true)

		case "unapproved", "unapproval":
			mergeRequestEntry.Approvalcount--
			mergeRequestEntry.UpdateApprovalCount(data.Db, false)

		case "reopen", "open":
			openNewMergeRequest = true

		case "merge":
			mergeRequestEntry.Closed = utils.MergeRequestMerged
			mergeRequestEntry.MarkDBEntryState(data.Db, utils.MergeRequestMerged)
			print("merge request marked as merged")

		case "close", "closed":
			mergeRequestEntry.Closed = utils.MergeRequestClosed
			mergeRequestEntry.DeleteMergeRequestMessage(data.Api, data.Db)
			print("merge request deleted")

		case "update":
			// update last commit id if it has changed since last save into database
			if lastCommit != mergeRequestEntry.LastCommit && lastCommit != "" {
				mergeRequestEntry.LastCommit = lastCommit
				mergeRequestEntry.Approvalcount = 0
				mergeRequestEntry.UpdateLastCommit(data.Db, lastCommit)
				mergeRequestEntry.ResetApprovalCount(data.Db)
			}

		default:
			// default
		}

		data.HandleMergeRequest(mergeRequestEntry, openNewMergeRequest)
	}
}

func (data *GitlabUtils) HandleMergeRequest(mergeRequestEntry *mergeRequest.MergeRequest, openNewMergeRequest bool) {
	// Only work with merge request if not already closed (and as such, deleted from the DB and Slack channel)
	if mergeRequestEntry.Closed != utils.MergeRequestClosed {
		var attachmentOption slack.MsgOption
		var buttonBlock *slack.ActionBlock = nil

		// Only build gitlab buttons if MR is open
		if mergeRequestEntry.Closed != utils.MergeRequestMerged {
			attachmentOption, buttonBlock = slackUtils.BuildMessageButtons(mergeRequestEntry, "")
		} else {
			// Do not generate buttons if merge request is already merged
			attachmentOption = slack.MsgOptionBlocks(slack.ActionBlock{})
		}

		// Send or update message in Slack channel
		if openNewMergeRequest {
			if buttonBlock == nil {
				mergeRequestEntry.SendMergeRequestMessage(data.Api, data.Db, attachmentOption)
			} else {
				mergeRequestEntry.SendMergeRequestMessage(data.Api, data.Db, attachmentOption, buttonBlock)
			}
		} else {
			if buttonBlock == nil {
				mergeRequestEntry.UpdateMergeRequestMessage(data.Api, data.Db, attachmentOption)
			} else {
				mergeRequestEntry.UpdateMergeRequestMessage(data.Api, data.Db, attachmentOption, buttonBlock)
			}
		}
	}
}

func (data *GitlabUtils) WebhookHandler(w http.ResponseWriter, r *http.Request) {
	hook, _ := gitlab.New(gitlab.Options.Secret(Settings.GitlabSecret))

	payload, err := hook.Parse(r, gitlab.MergeRequestEvents, gitlab.CommentEvents, gitlab.PipelineEvents)
	if err != nil {
		if err == gitlab.ErrEventNotFound {
			// Event wasn't one of the ones asked to be parsed
			fmt.Printf("event not found %s \n", err)
		}
	}

	data.WebhookDataHandler(payload)
}
