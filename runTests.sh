#!/bin/bash
# find package prefix
PACKAGE_PREFIX=$(go list -m -f '{{.Path}}')
# run 'go test' on all local packages
go test -v $(go list ${PACKAGE_PREFIX}/...)
