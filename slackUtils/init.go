package slackUtils

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/settings"
)

type SlackUtils struct {
	Api *slack.Client
	Db  *sql.DB
}

var Settings settings.Settings

func New(Api *slack.Client, Db *sql.DB) SlackUtils {
	return SlackUtils{
		Api: Api,
		Db:  Db,
	}
}

func init() {
	Settings = Settings.NewFromEnv()
}

func (data *SlackUtils) SetApi(Api *slack.Client) {
	data.Api = Api
}
func (data *SlackUtils) SetDb(Db *sql.DB) {
	data.Db = Db
}
