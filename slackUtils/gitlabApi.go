package slackUtils

import (
	"fmt"
	"github.com/hashicorp/go-retryablehttp"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

// SendGitlabRequest sends general gitlab API request and returns statusCode and responseBody
// @api
func sendGitlabRequest(projectId int, mergeRequestIID int, apiPath string, apiMethod string) (statusCode int, responseBody string) {
	// create new http client
	client := retryablehttp.NewClient()
	client.Backoff = retryablehttp.LinearJitterBackoff
	client.RetryWaitMin = 800 * time.Millisecond
	client.RetryWaitMax = 1200 * time.Millisecond
	client.RetryMax = 4
	client.ErrorHandler = retryablehttp.PassthroughErrorHandler

	// build request uri
	apiPath = strings.ReplaceAll(apiPath, ":project_id", strconv.Itoa(projectId))
	apiPath = strings.ReplaceAll(apiPath, ":merge_request_iid", strconv.Itoa(mergeRequestIID))
	var fullApiUrl = Settings.GitlabUrl + utils.ApiVersionUrlPart + apiPath

	// set the HTTP method, url, and request body
	req, err := http.NewRequest(apiMethod, fullApiUrl, nil)
	if err != nil {
		fmt.Println("Could not create new request: ", err)
	}

	req.Header.Set("Private-Token", Settings.GitlabAccessToken)

	// create retry request from original request
	retryReq, err := retryablehttp.FromRequest(req)
	if err != nil {
		fmt.Println("Could not create new retry request: ", err)
		return 0, ""
	}

	// send request
	resp, err := client.Do(retryReq)
	if err != nil {
		fmt.Println("Could not send request: ", err)
		return 0, ""
	}
	defer resp.Body.Close()

	fmt.Println("send merge request event to ", fullApiUrl)

	if resp.StatusCode != 200 {
		fmt.Println("starting merge failed with status code: ", resp.StatusCode)
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Could not read response: ", err)
	}
	fmt.Println("send request response body: ", string(bodyBytes))

	return resp.StatusCode, string(bodyBytes)
}
