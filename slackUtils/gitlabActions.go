package slackUtils

import (
	"encoding/json"
	"fmt"
	"gitlab.com/lsascha/gitlab-slackbot/mergeRequest"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"net/http"
	"net/url"
)

// MergeMRonGitlab merges a Gitlab MergeRequest and adds a comment to the Merge Request with the name of the merger (slack username)
func (data *SlackUtils) MergeMRonGitlab(projectId int, mergeRequestIID int, actionUser string) {
	returnStatusCode, responseBody := sendGitlabRequest(projectId, mergeRequestIID, "/projects/:project_id/merge_requests/:merge_request_iid/merge?merge_when_pipeline_succeeds=true", http.MethodPut)

	// try merge request again without option "merge_when_pipeline_succeeds" when 405 Method Not Allowed got returned from gitlab.
	// Probably effect from issue https://gitlab.com/gitlab-org/gitlab/issues/30953
	// @todo: if possible check if MR has a pipeline attached to use the correct API call directly without trying the other one first.
	if returnStatusCode == 405 {
		returnStatusCode, _ = sendGitlabRequest(projectId, mergeRequestIID, "/projects/:project_id/merge_requests/:merge_request_iid/merge", http.MethodPut)
	}
	if returnStatusCode == 200 {

		var payload MergeActionResponse
		err := json.Unmarshal([]byte(responseBody), &payload)
		if err != nil {
			fmt.Printf("Could not parse action response JSON: %v \n", err)
			return
		}
		_, mergeRequestObj := mergeRequest.GetFromDB(data.Db, Settings.ChannelId, payload.ID, "", payload.ProjectID)
		if payload.Pipeline.Status == "running" {

			attachmentOption, buttonBlock := BuildMessageButtons(mergeRequestObj, ":hourglass_flowing_sand:")

			mergeRequestObj.UpdateMergeRequestMessage(data.Api, data.Db, attachmentOption, buttonBlock)
		}
		mergeRequestObj.MarkDBEntryState(data.Db, utils.MergeRequestClosed)
		bodyText := utils.MergeRequestBotMergeCommentText + actionUser
		sendGitlabRequest(projectId, mergeRequestIID, "/projects/:project_id/merge_requests/:merge_request_iid/notes?body="+url.QueryEscape(bodyText), http.MethodPost)
	}
}
