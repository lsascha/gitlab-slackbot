package slackUtils

import (
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/mergeRequest"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"strconv"
	"strings"
)

func BuildMessageButtons(mergeRequest *mergeRequest.MergeRequest, mergeStatusEmoji string) (slack.MsgOption, *slack.ActionBlock) {

	if utils.FindRegexInStrings(utils.NoBrainMarkingRegex, []string{mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription}) {
		mergeRequest.NoBrainer = 1
	}
	if utils.FindRegexInStrings(utils.WipMarkingRegex, []string{mergeRequest.MergeRequestTitle, mergeRequest.MergeRequestDescription}) {
		mergeRequest.Wip = 1
	}
	// prefix if MR is nobrainer
	var messagePrefix = ""
	if mergeRequest.NoBrainer == 1 {
		messagePrefix = ":no-brainer:"
	}
	// add WIP emoji
	if mergeRequest.Wip == 1 {
		messagePrefix = ":hammer_and_wrench:" + messagePrefix
	}

	var actionBlock = &slack.ActionBlock{}

	// if access token and gitlab url is provided for direct merge feature
	if Settings.GitlabAccessToken != "" && Settings.GitlabUrl != "" {

		var blockButtons []slack.BlockElement

		btnValueArr := []string{
			"review",
			Settings.ChannelId,
			strconv.FormatInt(mergeRequest.MergeRequestId, 10),
			strconv.FormatInt(mergeRequest.MergeRequestIId, 10),
			strconv.FormatInt(mergeRequest.ProjectId, 10),
			strconv.FormatInt(mergeRequest.Approvalcount, 10),
			strconv.Itoa(mergeRequest.Reviews),
			mergeRequest.Assignee,
			mergeRequest.SourceBranch,
			mergeRequest.TargetBranch,
			messagePrefix,
			mergeRequest.MergeStatus,
		}

		if mergeRequest.MergeStatus == "can_be_merged" && Settings.DirectMerge {
			mergeConfirmationTitleTxt := slack.NewTextBlockObject("plain_text", "Are you sure?", false, false)
			mergeConfirmationTextTxt := slack.NewTextBlockObject("mrkdwn", "This will push the Merge Request from `"+mergeRequest.SourceBranch+"` into `"+mergeRequest.TargetBranch+"`.", false, false)
			mergeConfirmationConfirmBtnTxt := slack.NewTextBlockObject("plain_text", "Merge", false, false)
			mergeConfirmationDenyBtnTxt := slack.NewTextBlockObject("plain_text", "No!", false, false)
			mergeConfirmationBlock := slack.NewConfirmationBlockObject(mergeConfirmationTitleTxt, mergeConfirmationTextTxt, mergeConfirmationConfirmBtnTxt, mergeConfirmationDenyBtnTxt)

			var mergeBtnTextTxtStringArr []string
			mergeBtnTextTxtStringArr = append(mergeBtnTextTxtStringArr, "Direct Merge")
			if mergeStatusEmoji != "" {
				mergeBtnTextTxtStringArr = append(mergeBtnTextTxtStringArr, mergeStatusEmoji)
			}
			mergeBtnTextTxtString := strings.Join(mergeBtnTextTxtStringArr, " ")
			mergeBtnTxt := slack.NewTextBlockObject("plain_text", mergeBtnTextTxtString, true, false)
			mergeButton := slack.ButtonBlockElement{
				Type:     slack.METButton,
				Text:     mergeBtnTxt,
				ActionID: "merge",
				Value:    "merge;" + strconv.FormatInt(mergeRequest.ProjectId, 10) + ";" + strconv.FormatInt(mergeRequest.MergeRequestIId, 10) + ";" + strconv.FormatInt(mergeRequest.ProjectId, 10),
				Confirm:  mergeConfirmationBlock,
				Style:    slack.StylePrimary,
			}

			blockButtons = append(blockButtons, mergeButton)
		}

		reviewBtnTxt := slack.NewTextBlockObject("plain_text", ":eyes: Review", true, false)
		reviewButton := slack.ButtonBlockElement{
			Type:     slack.METButton,
			Text:     reviewBtnTxt,
			ActionID: "review",
			URL:      mergeRequest.MergeRequestUrl,
			Value:    strings.Join(btnValueArr, ";"),
		}

		blockButtons = append(blockButtons, reviewButton)

		actionBlock = slack.NewActionBlock("", blockButtons...)
	}

	attachmentOption := slack.MsgOptionBlocks(actionBlock)

	return attachmentOption, actionBlock
}
