package slackUtils

import (
	"encoding/json"
	"fmt"
	"github.com/go-playground/webhooks/v6/gitlab"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/mergeRequest"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"net/http"
	"strconv"
	"strings"
)

type PipelineResponse struct {
	ID     int64       `json:"id"`
	SHA    string      `json:"sha"`
	Ref    string      `json:"ref"`
	Status string      `json:"status"`
	Manual bool        `json:"manual"`
	User   gitlab.User `json:"user"`
	URL    string      `json:"web_url"`
}

// MergeActionResponse ObjectAttributes contains all the GitLab object attributes information
type MergeActionResponse struct {
	ID              int64             `json:"id"`
	Title           string            `json:"title"`
	ProjectID       int64             `json:"project_id"`
	Description     string            `json:"description"`
	State           string            `json:"state"`
	IID             int64             `json:"iid"`
	URL             string            `json:"web_url"`
	TargetBranch    string            `json:"target_branch"`
	SourceBranch    string            `json:"source_branch"`
	SourceProjectID int64             `json:"source_project_id"`
	TargetProjectID int64             `json:"target_project_id"`
	MergeStatus     string            `json:"merge_status"`
	SHA             string            `json:"sha"`
	WorkInProgress  bool              `json:"work_in_progress"`
	Assignee        gitlab.Assignee   `json:"assignee"`
	Assignees       []gitlab.Assignee `json:"assignees"`
	Conflicts       bool              `json:"has_conflicts"`
	Pipeline        PipelineResponse  `json:"pipeline"`
	HeadPipeline    PipelineResponse  `json:"head_pipeline"`
}

func (data *SlackUtils) SlackInteractionMessageHandling(btnName string, actionUser string, valueParts []string) {

	switch btnName {
	case "merge":
		btnValueType := ""
		btnValueProjectId := ""
		btnValueMergeRequestIID := ""
		if utils.IsSet(valueParts, 0) && valueParts[0] != "" {
			btnValueType = valueParts[0]
		}
		if utils.IsSet(valueParts, 1) && valueParts[1] != "" {
			btnValueProjectId = valueParts[1]
		}
		if utils.IsSet(valueParts, 2) && valueParts[2] != "" {
			btnValueMergeRequestIID = valueParts[2]
		}
		//if utils.IsSet(valueParts, 3) && valueParts[3] != "" {
		//	btnValueMergeRequestID = valueParts[3]
		//}
		if btnValueType != "" && btnValueProjectId != "" && btnValueMergeRequestIID != "" {
			btnValueProjectIdInt, _ := strconv.Atoi(btnValueProjectId)
			btnValueMergeRequestIIDInt, _ := strconv.Atoi(btnValueMergeRequestIID)
			// send request to merge in background
			go data.MergeMRonGitlab(btnValueProjectIdInt, btnValueMergeRequestIIDInt, actionUser)
		} else {
			fmt.Printf("Required payload Data wrong Type: %s ProjectID: %s MergeRequestIID: %s \n", btnValueType, btnValueProjectId, btnValueMergeRequestIID)
		}
	case "review":

		btnValueType := ""
		btnValueChannelId := ""
		btnValueMergeRequestId := ""
		btnValueProjectId := ""
		btnValueSourceBranch := ""
		btnValueTargetBranch := ""
		if utils.IsSet(valueParts, 0) && valueParts[0] != "" {
			btnValueType = valueParts[0]
		}
		if utils.IsSet(valueParts, 1) && valueParts[1] != "" {
			btnValueChannelId = valueParts[1]
		}
		if utils.IsSet(valueParts, 2) && valueParts[2] != "" {
			btnValueMergeRequestId = valueParts[2]
		}
		/*if utils.IsSet(valueParts, 3) && valueParts[3] != "" {
			btnValueMergeRequestIId = valueParts[3]
		}*/
		if utils.IsSet(valueParts, 4) && valueParts[4] != "" {
			btnValueProjectId = valueParts[4]
		}
		/*if utils.IsSet(valueParts, 5) && valueParts[5] != "" {
			btnValueApprovalcount = valueParts[5]
		}*/
		/*if utils.IsSet(valueParts, 6) && valueParts[6] != "" {
			btnValueReviews = valueParts[6]
		}*/
		/*if utils.IsSet(valueParts, 7) && valueParts[7] != "" {
			btnValueAssignee = valueParts[7]
		}*/
		if utils.IsSet(valueParts, 8) && valueParts[8] != "" {
			btnValueSourceBranch = valueParts[8]
		}
		if utils.IsSet(valueParts, 9) && valueParts[9] != "" {
			btnValueTargetBranch = valueParts[9]
		}
		/*if utils.IsSet(valueParts, 10) && valueParts[10] != "" {
			btnValueMessagePrefix = valueParts[10]
		}
		if utils.IsSet(valueParts, 11) && valueParts[11] != "" {
			btnValueMergeRequestStatus = valueParts[11]
		}*/
		if btnValueType != "" && btnValueChannelId != "" && btnValueMergeRequestId != "" && btnValueProjectId != "" {
			btnValueMergeRequestIdInt, _ := strconv.ParseInt(btnValueMergeRequestId, 10, 64)
			btnValueProjectIdInt, _ := strconv.ParseInt(btnValueProjectId, 10, 64)

			mergeRequestObj := mergeRequest.MergeRequest{
				ChannelId:      btnValueChannelId,
				MergeRequestId: btnValueMergeRequestIdInt,
				ProjectId:      btnValueProjectIdInt,
			}

			shouldUpdateMessage := mergeRequestObj.UpdateReviewCount(data.Db, actionUser, true)

			if shouldUpdateMessage {
				_, dbEntry := mergeRequest.GetFromDB(data.Db, mergeRequestObj.ChannelId, mergeRequestObj.MergeRequestId, "", mergeRequestObj.ProjectId)

				if dbEntry != nil {
					dbEntry.SourceBranch = btnValueSourceBranch
					dbEntry.TargetBranch = btnValueTargetBranch

					attachmentOption, buttonBlock := BuildMessageButtons(dbEntry, "")

					dbEntry.UpdateMergeRequestMessage(data.Api, data.Db, attachmentOption, buttonBlock)
				}
			}
		} else {
			fmt.Printf("Required payload Data wrong Type: %s btnValueChannelId: %s btnValueMergeRequestId: %s btnValueProjectId: %s \n", btnValueType, btnValueChannelId, btnValueMergeRequestId, btnValueProjectId)
		}

	}
}

func (data *SlackUtils) SlackActionHandler(w http.ResponseWriter, r *http.Request) {
	var payload slack.InteractionCallback

	fmt.Printf("action payload %s \n", r.FormValue("payload"))
	err := json.Unmarshal([]byte(r.FormValue("payload")), &payload)
	if err != nil {
		fmt.Printf("Could not parse action response JSON: %v \n", err)
		return
	}

	// send an OK header back as fast as possible to not hit the 3-second Slack answer timeout
	w.WriteHeader(http.StatusOK)

	if payload.Type == slack.InteractionTypeInteractionMessage {
		for _, attachmentAction := range payload.ActionCallback.AttachmentActions {
			btnName := attachmentAction.Name
			actionUser := payload.User.Name
			valueParts := strings.Split(attachmentAction.Value, ";")

			data.SlackInteractionMessageHandling(btnName, actionUser, valueParts)

			fmt.Printf("Message button %s pressed by user %s with value %s \n", btnName, actionUser, payload.Value)
		}
	}

	if payload.Type == slack.InteractionTypeBlockActions {
		for _, blockAction := range payload.ActionCallback.BlockActions {
			btnName := blockAction.ActionID
			actionUser := payload.User.Name
			valueParts := strings.Split(blockAction.Value, ";")

			data.SlackInteractionMessageHandling(btnName, actionUser, valueParts)

			fmt.Printf("Message button %s pressed by user %s with value %s \n", btnName, actionUser, payload.Value)
		}
	}

}
