# This file is a template, and might need editing before it works on your project.
FROM golang:1.20.6-alpine AS builder

WORKDIR /usr/src/app

RUN apk --no-cache add git gcc libc-dev

COPY . .
RUN go build -v -o /usr/src/app/app

FROM alpine:3.18

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/app/app .
CMD ["./app"]
