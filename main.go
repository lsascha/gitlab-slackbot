package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"github.com/slack-go/slack"
	"gitlab.com/lsascha/gitlab-slackbot/gitlabUtils"
	"gitlab.com/lsascha/gitlab-slackbot/settings"
	"gitlab.com/lsascha/gitlab-slackbot/slackUtils"
	"gitlab.com/lsascha/gitlab-slackbot/utils"
	"log"
	"net/http"
)

var Api *slack.Client
var Db *sql.DB
var Settings settings.Settings

func init() {
	Settings = Settings.NewFromEnv()
}

func main() {
	Api = slack.New(Settings.SlackToken)

	var err error
	Db, err = sql.Open("sqlite3", "file:"+Settings.DatabasePath+"/slackbot.db?mode=rwc&_journal_mode=WAL&_busy_timeout=5000")
	if err != nil {
		log.Fatal(err)
	}

	defer Db.Close()

	fmt.Println("Initializing Database")
	sqlStmt := `
		create table if not exists messages (id integer not null primary key, message text, channel_id text, msg_timestamp text, merge_request_id integer not null, merge_request_iid integer not null, merge_request_title text, merge_request_description text, merge_request_url text, project_id integer not null, repo_url text, approvalcount integer unsigned default 0, assignee text, closed integer unsigned default 0, merge_status text, reviews integer unsigned default 0, commented integer unsigned default 0, nobrainer integer unsigned default 0, pipeline_status text, last_commit text, merge_commit_sha text);
	`
	err = utils.ExecToDB(Db, sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}

	sqlStmt = `
		create table if not exists reviewer (id integer not null primary key, channel_id text, merge_request_id integer not null, project_id integer not null, reviewer_username text);
	`
	err = utils.ExecToDB(Db, sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}

	GitlabHandler := gitlabUtils.New(Api, Db)
	SlackHandler := slackUtils.New(Api, Db)

	http.HandleFunc(utils.Path, GitlabHandler.WebhookHandler)
	http.HandleFunc(utils.SlackActionHandlerPath, SlackHandler.SlackActionHandler)
	http.ListenAndServe(":3000", nil)
}
