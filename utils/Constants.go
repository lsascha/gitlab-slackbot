package utils

const (
	Path                   = "/webhooks"
	SlackActionHandlerPath = "/actions"
	ApiVersionUrlPart      = "/api/v4"
)

const (
	MergeRequestBotMergeCommentText = "Merge done through slackbot from user: "
	NoBrainMarkingRegex             = `(no.?brain|\[nb\])`
	WipMarkingRegex                 = `(draft:|wip:|\[wip\])`
)

const (
	MergeRequestOpen   = 0
	MergeRequestClosed = 1
	MergeRequestMerged = 2
)
