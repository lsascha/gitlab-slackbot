package utils

import (
	"database/sql"
	"errors"
	_ "github.com/mattn/go-sqlite3"
	"log"
	"net/url"
	"os"
	"reflect"
	"regexp"
	"strings"
)

func ExecToDB(db *sql.DB, query string, args ...interface{}) error {
	tx, err := db.Begin()
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Fatalf("update drivers: unable to rollback: %v", rollbackErr)
		}
		log.Fatal(err)
		return err
	}

	stmt, err := tx.Prepare(query)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Fatalf("update drivers: unable to rollback: %v", rollbackErr)
		}
		log.Fatal(err)
		return err
	}

	defer stmt.Close()

	_, err = stmt.Exec(args...)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Fatalf("update drivers: unable to rollback: %v", rollbackErr)
		}
		log.Fatal(err)
		return err
	}

	err = tx.Commit()
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			log.Fatalf("update drivers: unable to rollback: %v", rollbackErr)
		}
		log.Fatal(err)
		return err
	}
	return nil
}

// FileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func IsSet(arr []string, index int) bool {
	return len(arr) > index
}
func StringInSlice(a string, list []string, trim bool) bool {
	for _, b := range list {
		if trim {
			if strings.TrimSpace(b) == a {
				return true
			}
		} else {
			if b == a {
				return true
			}
		}
	}
	return false
}

func FindRegexInStrings(regex string, stringsArr []string) bool {
	for _, singleString := range stringsArr {
		matched, _ := regexp.MatchString(regex, strings.ToLower(singleString))
		if matched {
			return true
		}
	}
	return false
}

func GetIssueID(haystack string) ([]string, error) {
	re := regexp.MustCompile(`([A-Z]+-\d+)`)
	matches := re.FindAllString(haystack, -1)

	if len(matches) == 0 {
		return []string{""}, errors.New("no matches found in the string")
	}

	return matches, nil
}

type Issue struct {
	IssueId       string
	IssueUrl      string
	IssueMarkdown string
}

func containsIssueId(issues []Issue, IssueId string) bool {
	// iterate over the array and compare given string to each element
	for _, singleIssue := range issues {
		if singleIssue.IssueId == IssueId {
			return true
		}
	}
	return false
}

func GetIssueIdsWithUrlPrefixed(issueIds []string, urlPrefix string) []Issue {
	var resultIssues []Issue
	for _, singleIssueId := range issueIds {
		fullUrl, _ := url.JoinPath(urlPrefix, singleIssueId)
		if !containsIssueId(resultIssues, singleIssueId) {
			resultIssues = append(resultIssues, Issue{singleIssueId, fullUrl, "<" + fullUrl + "|*" + singleIssueId + "*>"})
		}
	}

	return resultIssues
}
func GetListOfIssuesAsMarkdown(issues []Issue) string {
	var issueMarkdownList []string
	for _, issue := range issues {
		issueMarkdownList = append(issueMarkdownList, issue.IssueMarkdown)
	}

	return strings.Join(issueMarkdownList, ",")
}

func Clone(oldObj interface{}) interface{} {
	newObj := reflect.New(reflect.TypeOf(oldObj).Elem())
	oldVal := reflect.ValueOf(oldObj).Elem()
	newVal := newObj.Elem()
	for i := 0; i < oldVal.NumField(); i++ {
		newValField := newVal.Field(i)
		if newValField.CanSet() {
			newValField.Set(oldVal.Field(i))
		}
	}

	return newObj.Interface()
}

func ReplaceStrings(s string) string {
	// Get the replace pairs from environment variable
	replacePairs := strings.Split(os.Getenv("REPLACE_PAIRS"), ",")

	// Process each replace pair
	for _, pair := range replacePairs {
		// Skip if pair is empty
		if pair == "" {
			continue
		}

		// Split the pair into the string to replace and the replacement
		parts := strings.Split(pair, ":")
		if len(parts) != 2 {
			panic("Invalid replace pair: " + pair)
		}

		// Replace the string
		s = strings.ReplaceAll(s, parts[0], parts[1])
	}

	return s
}
