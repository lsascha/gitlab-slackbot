package utils

import (
	"testing"
)

func TestGetIssueID(t *testing.T) {
	testIssueIdString := "test ABC-123 ticket"
	testIssueMatchString := "ABC-123"
	resultString, _ := GetIssueID(testIssueIdString)

	if resultString[0] != testIssueMatchString {
		t.Fatalf(`GetIssueID(string) = %q, want match for %#q`, resultString, testIssueMatchString)
	}
}
