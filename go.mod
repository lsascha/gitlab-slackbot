module gitlab.com/lsascha/gitlab-slackbot

go 1.20

replace github.com/go-playground/webhooks/v6 => github.com/lsascha/webhooks/v6 v6.1.1

require (
	github.com/go-playground/webhooks/v6 v6.2.0
	github.com/hashicorp/go-retryablehttp v0.7.4
	github.com/joho/godotenv v1.5.1
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/slack-go/slack v0.12.2
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
)
