package settings

import (
	"github.com/joho/godotenv"
	"log"
	"os"
	"strings"
)

type Settings struct {
	SlackToken           string // SlackToken Set your "Bot User OAuth Access Token" which starts with "xoxb-"
	ChannelId            string
	GitlabSecret         string // GitlabSecret Secret provided in hook calls from gitlab
	GitlabAccessToken    string
	GitlabUrl            string
	DirectMerge          bool
	IgnoredUsernames     string
	MasterLiveBranchName string
	DatabasePath         string
}

func (settings *Settings) New() Settings {
	return Settings{
		SlackToken:           "",
		ChannelId:            "",
		GitlabSecret:         "",
		GitlabAccessToken:    "",
		GitlabUrl:            "",
		IgnoredUsernames:     "",
		MasterLiveBranchName: "master",
		DatabasePath:         "./db",
		DirectMerge:          false,
	}
}

func (settings *Settings) NewFromEnv() Settings {
	_ = godotenv.Load()

	newSettings := Settings{
		SlackToken:           os.Getenv("SLACK_TOKEN"),
		ChannelId:            os.Getenv("SLACK_CHANNEL_ID"),
		GitlabSecret:         os.Getenv("GITLAB_SECRET"),
		GitlabAccessToken:    os.Getenv("GITLAB_ACCESS_TOKEN"),
		GitlabUrl:            os.Getenv("GITLAB_URL"),
		DirectMerge:          strings.ToLower(os.Getenv("DIRECT_MERGE")) == "true" || os.Getenv("DIRECT_MERGE") == "1",
		IgnoredUsernames:     os.Getenv("GITLAB_IGNORED_USERNAMES"),
		MasterLiveBranchName: os.Getenv("MASTER_LIVE_BRANCH_NAME"),
		DatabasePath:         os.Getenv("DB_PATH"),
	}
	if newSettings.MasterLiveBranchName == "" {
		newSettings.MasterLiveBranchName = "master"
	}
	if newSettings.DatabasePath == "" {
		newSettings.DatabasePath = "./db"
	}

	// Make sure the directory for the database exists
	_ = os.MkdirAll(newSettings.DatabasePath, 0644)

	if newSettings.SlackToken == "" || newSettings.ChannelId == "" || newSettings.GitlabSecret == "" {
		log.Fatal("Not all required environment variables set.")
	}

	return newSettings
}

func (settings *Settings) GetIgnoredUsernamesSlice() []string {
	return strings.Split(settings.IgnoredUsernames, ",")
}
