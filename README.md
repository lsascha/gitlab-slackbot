# Gitlab Slackbot

Creates Gitlab Merge Request Messages in a Slack Channel.

![Example Merge Request Message in Slack from Gitlab](docs/img/slackbot-gitlab-mr-example.png)

## Bot Configuration

All Configurations are made using Environment Variables.

* Bot User OAuth Access Token (starts with "xoxb-")

  `SLACK_TOKEN=`

* use Alphanumeric ID inside the Link to the Channel. (last part of `https://somename.slack.com/archives/CTY9R28F3` so
  it is `CTY9R28F3`)
  you can get it by right-clicking on the channel and choosing "Copy channel-link"

  `SLACK_CHANNEL_ID=`

* Comma separated list of String replace pairs. (useful to replace special characters used in MR titles [for example in renovate the =>]) where a `:` is used as separator.
  
  _(example: `REPLACE_PAIRS==>:to` will replace all `=>` with `to`)_

  `REPLACE_PAIRS=`

* Secret provided in hook calls from gitlab

  `GITLAB_SECRET=`

* Gitlab personal access token (requires API scope)

  `GITLAB_ACCESS_TOKEN=`

* URL to Gitlab instance ("https://gitlab.com" for public gitlab) without "/" at the end.

  `GITLAB_URL=`

* Comma separated list of Usernames, gitlab comments are ignored from. (useful for comments from Bots)

  `GITLAB_IGNORED_USERNAMES=`

* "True" or "1" if the bot should add the Direct Merge Button to the message. (requires `GITLAB_ACCESS_TOKEN` and `GITLAB_URL`)

  `DIRECT_MERGE=`

`GITLAB_ACCESS_TOKEN` and `GITLAB_URL` are only used for the "Direct Merge" and "review" Buttons in the message.
If one of these are empty, no button will be added to the message.

## Gitlab Configuration

* Gitlab Hook URL needs to point to the URL `https://hostname-of-bot/webhooks`. which needs to be accessible from the
  Internet.

* Add some random string to the Hooks `Secret Token` and add the same to the environment variable `GITLAB_SECRET`

* The Hook in Gitlab needs to listen to `Comments` (called Note events in API), `Merge requests events`
  and `Pipeline events`.

* _Optional for the "Direct Merge" Button_:
    * Create a User Access Token in the User Settings under `Access Tokens` (
      url `https://gitlab.com/profile/personal_access_tokens`).
    * Give it the Scope `api`.
    * The User of the `GITLAB_ACCESS_TOKEN` needs permissions to merge in the Repository for the Button work.

## Slack Configuration (`https://api.slack.com/`)

* Create the Slack Token using Link https://api.slack.com/apps?new_classic_app=1 (You can `Update to Granular Scopes`
  later. After Scope Updates you need to `Reinstall App`)

    * Required Scopes are `chat:write`.

      _(TODO: confirm that this is enough. might require more Scopes)._

* Add the `Bot User OAuth Access Token` (starts with "xoxb-") to the `SLACK_TOKEN` environment variable (
  see [Bot Configuration](#bot-configuration))

* Finally, add the Bot User to the Slack Channel you want the Bot to Post to and add set the ID of the Slack channel to
  the `SLACK_CHANNEL_ID` environment variable.
    * You can get the Slack Channel ID by right-clicking on the channel and choosing "Copy channel-link". The last part
      that looks like `CTY9R28F3` is the ID.

* _Optional for the "Direct Merge" Button_:
    * Url for Slack Action Events `https://hostname-of-bot/actions` needs to be accessible from the Internet if the "
      Direct Merge" Button should be used.
    * Under `Interactive Components` Enable `Interactivity` and add the `https://hostname-of-bot/actions`
      as `Request URL`

## Used Emojis inside the Slack Messages

Since currently the messages can not be configured, make sure the following emojis are available in Slack

- `:bust_in_silhouette:` _(Slack Default)_
- `:anatomical_heart:` _(Slack Default)_
- `:hammer_and_wrench:` _(Slack Default)_
- `:heavy_check_mark:` _(Slack Default)_
- `:eyes:` _(Slack Default)_
- `:speech_balloon:` _(Slack Default)_
- `:no-brainer:` _(Recommended: <img src="docs/img/emoji/no-brainer.png" width="22" height="22" />)_
- `:merged:` _(Recommended: <img src="docs/img/emoji/merged.png" width="22" height="22" />)_
